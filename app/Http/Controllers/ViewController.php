<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\View\View;

class ViewController extends BaseController
{
    /**
     * Show the page.
     *
     * @return View
     */
    public function show()
    {
        return view('welcome', ['data' => 'test123']);
    }
}
