<?php

namespace App\Http\Controllers;

use App\Models\User;
use \Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;

class ApiController extends BaseController
{
    /**
     * The user model.
     *
     * @var User
     */
    protected $users;

    /**
     * Create a new controller instance.
     *
     * @param User $users
     * @return void
     */
    public function __construct(User $users)
    {
        /* In most cases, one would probably want to inject a
           repository to extract the database logic from the controller.
           In this case, there is not enough complexity for it. */
        $this->users = $users;
    }

    /**
     * Create a new resource.
     * TODO: add validation with custom Request
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $user = $this->users->create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        // TODO: make actual Resource for response
        return response()->json($user->toArray());
    }

    /**
     * Update the specified existing resource. Route model binding
     * gives us the User model using the id passed into the route.
     * TODO: add validation with custom Request
     *
     * @param Request $request
     * @param User $user
     *
     * @return JsonResponse
     */
    public function update(Request $request, User $user)
    {
        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        // TODO: make actual Resource for response
        return response()->json($user->toArray());
    }
}
