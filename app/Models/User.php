<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'is_cool'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Filters the query to only include cool users.
     *
     * @param Builder
     *
     * @return Builder
     */
    public function scopeCool(Builder $query)
    {
        return $query->where('is_cool', true);
    }

    // TODO: add example relationships
    /* TODO: add other example model methods with enough
       complexity to demonstrate breaking up methods into smaller
       methods to prevent duplication and/or for readability. */
}
