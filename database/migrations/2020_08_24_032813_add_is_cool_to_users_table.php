<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsCoolToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('users') && !Schema::hasColumn('users', 'is_cool')) {
            Schema::table('users', function (Blueprint $table) {
                $table->boolean('is_cool')->default(false);
            });

            /* of course a data migration would only make sense if there
               is existing data when the new column is added */
            optional(User::where('email', 'derekjpaul@gmail.com')->first())
                ->update(['is_cool' => true]);
        }
    }
}
